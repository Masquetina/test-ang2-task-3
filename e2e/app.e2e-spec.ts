import { LocalizeAppPage } from './app.po';

describe('localize-app App', () => {
  let page: LocalizeAppPage;

  beforeEach(() => {
    page = new LocalizeAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
